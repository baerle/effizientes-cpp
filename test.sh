#!/bin/bash

# syntax: ./test.sh <file.h> <arguments>

createdNewFile=false
file=${1##*/}
echo $file
filename=${file%.h}
echo $filename
path=${1%$file}
if [[ $file != $filename ]]; then
  newPath="$path$filename.cpp"
  echo "Path: $newPath"

  cat $1 > $newPath
  createdNewFile=true
else  #.cpp
  newPath="$path$file"
fi
g++ -Wall -std=c++11 ${*:2} $newPath

echo -e "\n\n"
./a.out #${*:2}
rm ./a.out
if [[ $createdNewFile == true ]]; then
  rm $newPath
fi