#include <iostream>

using namespace std;

class A {
    friend int main();

    int a = 1;
};

class B : public A {
    int a = 2;
};

int main() {
    B a;

    cout << static_cast<A>a.a << endl;  //besser: dynamic_cast  -> zusaetzlich Typueberpruefung zur Laufzeit

    return 0;
}