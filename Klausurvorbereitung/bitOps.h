#include <iostream>
#include <sstream>

using namespace std;

void print(ushort c) {
    stringstream s;
    s << hex << c;
    cout << s.str() << endl;
}

int main() {
    ushort a = 0b01100101;
    ushort b = 0b11001011;

    print(~a); // 10011010 -> invertieren
    print(-a); // 10011011 -> negieren
    print((a & b)); // 01000001 -> und (and)
    print((a | b)); // 11101111 -> oder (or)
    print((a ^ b)); // 10101110 -> exklusives oder (xor)
}