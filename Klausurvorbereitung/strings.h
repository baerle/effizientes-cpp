#include <iostream>
#include <string>

using namespace std;

int main(int argc, char *argv[]) {
    for (int i = 0; i < argc; ++i) {
        cout << argv[i] << endl;
    }

    cout << u8"UTF-8-String" << ' ' << u"UTF-16-String" << ' ' <<  U"UTF-32-String" << ' ' <<  L"wchar_t-String" << endl;

    string str = R"(Dies ist ein sogenannter roher-String, \t er wird durch ein Null-Byte terminiert!)";
    string str2 = R"(C:\Windows\System32\drivers\etc\hosts)";

    for (uint i {0}; i < str.size(); i++) cout << str[i];
    cout << endl;
    for (uint i = 0; str2[i]; cout << str2[i++]);
    cout << endl;

    return 0;
}