#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <vector>
#include <regex>

using namespace std;

static vector<string> nodes;

static vector<string> node;
static int **matrix = nullptr;
static int *path = nullptr, *minPath = nullptr;
static bool *visited = nullptr;
static int dim, minWaylength = 0;

void uebung41() {
    static const int N{100};

    bool gefaengnis[N]{};

    for (bool zelle : gefaengnis)
        cout << zelle;
    cout << endl << endl;

    for (int i = 1; i <= N; ++i) {
        int counter{i};
        for (int j = 0; j < N; ++j) {
            if (--counter == 0) {
                gefaengnis[j] = !gefaengnis[j];
                counter = i;
            }
        }
    }

    int opened{0};
    for (bool zelle : gefaengnis) {
        cout << zelle;
        if (zelle) opened++;
    }
    cout << endl << endl << "offen: " << opened << endl;
}

/*int main() {
    uebung41();
}*/

bool nodes_remaining(int *visited, int size) {
    for (int i = 0; i < size; i++) {
        if (visited[i] == -1) return true;
    }
    return false;
}

void uebung42() {
    //Hamilton-Graph

    ifstream file{"../Übung4/adjazenzmatrix.txt"};
    if (file.bad()) {
        cerr << "Kann die Datei nicht einlesen!" << endl;
        exit(1);
    }

    string line;
    getline(file, line);

    stringstream str{line};

    // Parse die Knotennamen in einen Vektor: Trennzeichen ist das Leerzeichen
    while (str.good()) {
        string &&s{};
        str >> s;
        nodes.push_back(s);
    }

    for (uint i = 0; i < nodes.size(); i++) {
        cout << nodes[i] << ' ';
    }
    cout << endl;

    // Aufbau der Distanzmatrix (gewichtete Adjazenzmatrix)
    // Werte liegen in Datei in Dreiecksform vor
    // Ergebnis soll quadratische Matrix sein
    const int dim = nodes.size();
    int matrix[dim][dim];

    // Lese weitere Zeilen der Datei
    for (int i = 0; i < dim; i++) {
        // Lese nur die untere Dreiecksmatrix, die Hauptdiagonale steht nicht mehr in der Datei
        for (int j = 0; j < i; j++) {
            file >> matrix[i][j];
            matrix[j][i] = matrix[i][j];
        }
        // Von i nach i ist die Entfernung immer 0 (Hauptdiagonale)
        matrix[i][i] = 0;
    }

    /*for (int i = 0; i < dim; i++) {
        for (int j = 0; j < i; j++) {
            cout << matrix[i][j] << ' ';
        }
        cout << endl;
    }*/

    file.close();

    // Algorithmus: "Nächster Nachbar"
    // Ausgehend von einem gewählten Startknoten finde die kürzeste Strecke zu einem beliebigen anderen *noch nicht besuchten*
    // Knoten und besuche diesen Knoten. Wiederhole bis alle Knoten besucht sind, dann direkt zurück zum Startknoten
    // Hilfsmittel: Mittzeichnen welche Knoten besucht sind (z.B. Strichliste mit Knotennummern)
    int distances[dim];
    for (int k = 0; k < dim; k++) {

        int visited[dim];
        for (uint i = 0; i < dim; i++) visited[i] = -1;
        int orderVisited[dim + 1];

        //Startknoten wählen
        int start{k};
        visited[k] = 0;
        orderVisited[0] = k;
        int last{k};

        int count{0};
        int distance{0};

        do {
            // Finde kürzeste Strecke := Minimum suchen
            int min{INT16_MAX};
            int next;
            for (int i = 0; i < dim; i++) {
                if (visited[i] == -1 && matrix[last][i] < min) {
                    min = matrix[last][i];
                    next = i;
                }
            }

            visited[next] = ++count;
            distance += min;
            orderVisited[count] = next;

            //cout << "Von: " << nodes[last] << " Zu: " << nodes[next] << " Entfernung: " << min << endl;

            last = next;

        } while (nodes_remaining(visited, dim));

        distance += matrix[last][start];
        orderVisited[dim] = 0;
        distances[k] = distance;
        //cout << "Von: " << nodes[last] << " Zu: " << nodes[start] << " Entfernung: " << matrix[last][start] << endl;

        for (uint i = 0; i < dim; i++) {
            cout << nodes[orderVisited[i]] << " -> " << matrix[orderVisited[i]][orderVisited[i + 1]] << " -> "
                 << nodes[orderVisited[i + 1]] << endl;
        }
        cout << endl << "Gesamtdistanz: " << distance << endl << endl;
    }

    for (int i = 0; i < dim; i++) {
        cout << distances[i] << endl;
    }
}

int not_visited() {
    int rest = 0;
    for (int i = 0; i < dim; i++) {
        if (!visited[i]) rest++;
    }
    return rest;
}

int next(int index) {
    for (int i = index; i < dim + index; i++) {
        if (!visited[i % dim]) return i % dim;
    }
    // benötigt return-Wert für Compiler
    return -1;  // dies sollte nie erreicht werden --> Fehler
}

void print() {
    cout << node[minPath[0]] << " ";
    for (int i = 1; i < dim; i++) {
        cout << node[minPath[i]] << " ";
    }
    cout << minWaylength << endl;
}

void calculate_path() {
    int waylength = 0;
    cout << node[path[0]] << " ";
    for (int i = 1; i < dim; i++) {
        // Falls ein Fehler auftritt, abbruch
        if (path[i] == -1) break;
        waylength += matrix[path[i - 1]][path[i]];
        cout << node[path[i]] << " ";
    }
    waylength += matrix[path[dim - 1]][path[0]];
    cout << waylength << endl;

    if (minWaylength == 0 || waylength < minWaylength) {
        minWaylength = waylength;
        for (int i = 0; i < dim; i++) {
            minPath[i] = path[i];
        }
    }
}

void round_trip(int index) {
    static int deep = 0;
    // In Schritt deep, besuche Knoten index
    path[deep++] = index;
    visited[index] = true;

    int rest = not_visited();
    // Rekursionsabbruch
    if (rest == 0) {
        // Lösung berechnen, ausgeben
        calculate_path();
    } else {
        for (int i = 0, k = index; i < rest; i++) {
            // gehe zum nächsten unbesuchten Knoten
            round_trip(k = next(k + 1));
        }
    }
    // Freigabe des Knotens für weitere Rundwege
    visited[index] = false;
    path[--deep] = -1;
}

void uebung422() {
    //Hamilton-Graph

    ifstream file{"../Übung4/adjazenzmatrix.txt"};
    if (file.bad()) {
        cerr << "Kann die Datei nicht einlesen!" << endl;
        exit(1);
    }

    string line;
    getline(file, line);

    stringstream str{line};

    // Parse die Knotennamen in einen Vektor: Trennzeichen ist das Leerzeichen
    while (str.good()) {
        string &&s{};
        str >> s;
        nodes.push_back(s);
    }

    for (uint i = 0; i < nodes.size(); i++) {
        cout << nodes[i] << ' ';
    }
    cout << endl;

    // Aufbau der Distanzmatrix (gewichtete Adjazenzmatrix)
    // Werte liegen in Datei in Dreiecksform vor
    // Ergebnis soll quadratische Matrix sein
    dim = nodes.size();
    matrix = new int *[dim];

    // Lese weitere Zeilen der Datei
    for (int i = 0; i < dim; i++) {
        matrix[i] = new int[dim];
        // Lese nur die untere Dreiecksmatrix, die Hauptdiagonale steht nicht mehr in der Datei
        for (int j = 0; j < i; j++) {
            file >> matrix[i][j];
            matrix[j][i] = matrix[i][j];
        }
        // Von i nach i ist die Entfernung immer 0 (Hauptdiagonale)
        matrix[i][i] = 0;
    }

    /*for (int i = 0; i < dim; i++) {
        for (int j = 0; j < i; j++) {
            cout << matrix[i][j] << ' ';
        }
        cout << endl;
    }*/

    file.close();

    /*
     * Wir suchen einen Algorithmus, der alle Rundwege berechnet.
     * Die Fakultät spielt dabei eiene Rolle
     * Geg.: Ein vollständiger Graph mit n Knoten
     * Ausgehend von einem gewählten Knoten, besuche alle (n-1) anderen Knoten als nächstes,
     * danach, ausgehend von Knoten (n-1) besuche alle (n-2) Knoten usw.
     * Ausgehend von Knoten 2 besuche den letzten noch übrigen Knoten und gehe zurück auf Start.
     */
    path = new int[dim];
    minPath = new int[dim];
    visited = new bool[dim];
    for (int a = 0; a < dim; a++) {
        path[a] = -1;
        visited[a] = false;
    }

    round_trip(0);

    delete[] path;
    delete[] visited;
    for (int i = 0; i < dim; i++) delete[] matrix[i];
    delete[] matrix;
}

void uebung43() {
    string input;
    getline(cin, input);

    for (int i = 0; i < input.length(); ++i) {
        string in{"lew"};
        switch (input[i]) {
            case 'a':
            case 'e':
            case 'i':
            case 'o':
            case 'u':
                in.append(1, (char) input[i]);
                input.insert(i + 1, in);
                i += 4;
                break;
        }    }

    cout << input << endl;
}

void uebung44() {
    uint n;
    cin >> n;

    string calc{to_string(1.0 / n)};

    int indexLastZero = 0;
    for (uint i = 2; i < calc.length(); i++) {
        if (calc[i] == '0') {
            if (indexLastZero == 0)
                indexLastZero = i;
        } else {
            if (indexLastZero != 0)
                indexLastZero = 0;
        }
    }

    cout << calc << ": " << calc.length() << " : " << indexLastZero << " : ";
    if (indexLastZero > 2)
        cout << indexLastZero - 2 << " Nachkommastellen" << endl;
    else if (indexLastZero == 0) {
        cout << "Periode" << endl;
        smatch m;
        regex e("([0-9]+)+\1$");
        if (regex_search(calc, m, e, regex_constants::match_any)) { // not working
            cout << "here" << endl;
            for (auto x : m) cout << x << " " << endl;
        } else cout << "else" << endl;
    } else cout << "keine Nachkommastellen" << endl;
}

void uebung442() { // Lösung von Andreas
    uint n;
    cin >> n;

    int zweien = 0;
    while (n% 2 == 0) {
        n/=2;
        zweien++;
    }

    int fuenfen = 0;
    while (n%5 == 0) {
        n/= 5;
        fuenfen++;
    }

    if (n == 1) {
        cout << (zweien > fuenfen ? zweien : fuenfen) << endl;
    }

    int ziffern = 0;
    int result;
    long tmp = 10;
    do {
        result = tmp % n;
        tmp = result * 10;
        ziffern++;
    } while (result != 1);

    cout << ziffern << endl;
}

void uebung443() {  // Lösung von Schaller (auch falsch)
    uint n;
    cin >> n;
    uint z = 1;
    vector<int> v {};
    vector<int> p {};

    int sentinel = 1000;
    uint zeros = 0;
    do {
        while (z < n) {
            z *= 10;
            v.push_back(0);
            zeros++;
        }

        v.push_back(z / n);
        z %= n;
        bool period = false;
        for (auto k : p) {
            if (k == z) period = true;
        }
        if (period) break;
        p.push_back(z);
        z *= 10;
    } while (z && !--sentinel);

    cout << v[0] << ',';
    for (uint i = 1; i < v.size(); i++) {
        cout << v[i];
    }
    cout << endl;
    cout << "Länge: " << p.size()+zeros << endl;
}