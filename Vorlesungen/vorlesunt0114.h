#include <iostream>
#include "namespace1.h"
#include "namespace2.h"

using namespace std;

namespace {
    struct Data {
        double value;
    };

    void print(double d) {
        cout << d << endl;
    }
}

struct Mystruct {
    int value;

    explicit Mystruct(int value) : value(value) {}

    bool invalid() {
        return value < 0;
    }
};

pair<int*, int> allocate(int v) {
    int *p = nullptr;
    int error = v <= 0;

    if (!error) p = new int[v];
    return pair<int*, int>{p, error};
}

namespace std {
    int x = 1;
    using My::out;
}
int x = 2;

int main() {
    try {
        Mystruct m {-1};
        if (m.invalid()) throw exception{};
    } catch (exception &e) {
        cerr << e.what() << endl;
    } catch (...) {
        cerr << "This should never happen!" << endl;
    }

    pair<int*, int> retval = allocate(10);
    if (retval.second) cerr << "Fehler!" << retval.second << endl;
    else delete[] retval.first;

    using myout = My::out;
    myout myout1 {15};
    out out1 {51};

    namespace Myns = My;

    cout << Myns::s << myout1.value << out1.value << My::i << ::x << std::x << endl;

    using namespace My;
    cout << f() << V_2_0::f() << endl;

    Data d {1.3};
    print(d.value);
    print((int)d.value);
}