#include <iostream>
#include <string>
#include <type_traits>

using namespace std;

// Vorwärtsdeklaration, die Typdefinition kann an anderer Stelle erfolgen
// reine Deklaration des Bezeichners Kontakt als Datenstruktur; bekannt ist dem Compiler ab jetzt deren Name, unbekannt ist die Speichergröße
struct Kontakt;

struct Kontakt {
    string name;
    string str;
    int nr; //fängt an einer Wortgrenze an (Wortlänge == sizeof(int))
    char plz[6]; // 3 Bytes verschenkt
    string ort;

    Kontakt(string n, string s, int r, string p, string o): name(n), str(s), nr(r), ort(o) {
        for (uint i = 0; i < p.size() && i < 6; i++) {
            plz[i] = p[i];
        }
    }
    
    void print();
};

struct Data {
    unsigned char bits:2;
    char wahr:1;
    char falsch:1;
    unsigned char rest:4;
};

// Fall ein Member außerhalb der Datenstruktur definiert ist, muss der Name der Datenstruktur gefolgt vom Bereichsoperator(::) vorangestellt werden.
// Grundprinzip: "Jede Klasse ist ein Namensraum."
void Kontakt::print() {
    cout << name << endl
         << str << ' ' << nr << endl
         << plz << ' ' << ort << endl;
}

void print(uint* c) {
    cout << *c << endl;
}

void vorlesung1126() {
    // die PLZ hat zwar 5-Zeichen, aber die automatische Abbildung auf char[] aus C-String hat 0-Byte am Ende
    Kontakt k {"Patrick Bär", "Proß", 41, "95336", "Mainleus"};
    Kontakt *pk = &k;
    
    Kontakt *heapKontakt = new Kontakt("", "", 0, "", ""); // belege Speicher für einen Kontakt auf dem Heap
    
    k.nr = 1;
    (*pk).plz[0] = '9';
    pk->plz[1] = '5';
    k.plz[2] = '0';
    k.plz[3] = '2';
    k.plz[4] = '8';
    
    k.print();

    cout << endl << "POD: " << is_pod<Data>::value << endl;

    Data d {};

    d.bits = 3;
    d.wahr = 1;
    d.falsch = 1;
    d.rest = 15;

    print((uint*)&d);
}

/*int main(){
    vorlesung1126();
    return 0;
}*/