#include <iostream>

using namespace std;

void swapPointer(int *a, int *b) {
    int t = *a;
    *a = *b;
    *b = t;

    cout << "in swap: " << *a << ' ' << *b << endl;
}

void swapReference(int &a, int &b) {
    int t = static_cast<int&&>(a);  //der alte Speicher wird jeweils nicht mehr gebraucht
    a = move(b); //alias für static_cast
    b = static_cast<int&&>(t);

    cout << "in Reference: " << a << ' ' << b << endl;
}

void vorlesung1119() {
    int u {3}, v {4};
    cout << "in main: " << u << ' ' << v << endl;

    swapPointer(&u, &v);
    swapReference(u, v);

    cout << "in main: " << u << ' ' << v << endl;

    int a = 5, b = 6;

    //binden einer Referenz an ein Objekt im Speicher
    const int &ra = a, &rb = b;

    //a++;
    //ra++;

    cout << ra << " " << rb << endl; //automatisch dereferenziert von ra liefert Wert von a

    swapPointer(&a,&b);
    //swapPointer(&ra, &rb);
    cout << ra << " " << rb << endl;
}

/*int main() {
    vorlesung1119();
    return 0;
}*/