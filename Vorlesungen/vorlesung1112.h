#include <iostream>

using namespace std;

int x[10]; //global (lebt, bis Programm am Ende)

void arrays(int size) {
    int s[10]; //lokal auf Stack

    int k[size]; //dynamisches lokales Array auf dem Stack (mit Schleife initialisiert werden)

    int *p = new int[size]; //dynamisches Array auf dem Heap

}// wird die Funktion beendet, verwinden die lokalen Arrays, nur Daten auf dem Heap leben bis zum Aufruf von delete

//int main() {
    const int size {5};  // =^ #define size 5
    int k[size] = {1,2,3,4,5};
    int l[size] {1,2,3,4,5};

    for (int i = 0; i < size; ++i) {
        cout << k[i] << " : " << l[i] << endl;
    }

    int a[][2] {{1,2}, {3,4}, {5,6}};
    for (int i = 0; i < 3; ++i) {
        for (int j = 0; j < 2; ++j) {
            cout << a[i][j] << " ";
        }
        cout << endl;
    }
    cout << endl;

    for (auto & i : a) {
        for (int j : i) {
            cout << j << " ";
        }
        cout << endl;
    }
    cout << endl << endl;

    const int r {10};

    int *q = (int*) &r;

    //p erhält die Adresse von r
    int *p = (int*) &r;
    //gehe an die Adresse von p und schreibe dort 5 hinein
    *p = 5;

    cout << p << ": " << *p << endl;

    cout << q << ": " << *q << endl;
    cout << &r << ": " << r << endl << endl << endl;

    // const-ptr auf const char
    const char *const t = "Konstante\\0 Zeichenkette";
    cout << t << endl;


    return 0;
//}
