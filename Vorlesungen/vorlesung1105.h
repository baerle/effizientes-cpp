#include <iostream>

using namespace std;

void vorlesung1105() {
    // Ein statisches Array mit 10 Elementen, alle genullt  (auf dem Stack)
    int a[3] {0x30313233, 0x34353637, 0x38393a3b};

    // Ein Array in C/C++ wird automatisch zum Zeiger auf sein erstes Element
    // Der Zeiger p kennt die Größe des Arrays nicht!
    char *p = (char*) a;    //a+3  <-- Array Index out of Bounds

    // p und a sind nun beide (fast) das gleiche,
    // nämlich Zeiger auf das erste Element in einem Integer-Array mit 10 Elementen

    for (auto i : a) cout << i << endl; cout << endl << endl;

    for (uint i = 0; i < sizeof(a)/sizeof(char); i++) {
        cout << p[i] << ": " << *(a+i) << endl;
    }

    // Ein dynamisches Array auf dem Heap wird mit new erzeugt
    //int b[] = new int[3];

    // Jetzt ist b von Zeigertyp. Die Größe des Arrays ist dem Comiler dann nicht bekannt
    int *c = new int[3];

    for (int i = 0; i < 3; ++i) {
        cout << a[i] << endl;
    }

    delete[] c;
}