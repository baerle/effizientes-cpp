//
// Created by patrick on 1/14/21.
//

#ifndef C___NAMESPACE1_H
#define C___NAMESPACE1_H

#include <string>

namespace My {
    using namespace std;

    string s {"My namespace"};

    void print(int x) {
        cout << x << endl;
    }
}

#endif //C___NAMESPACE1_H
