#include <iostream>

using namespace std;

constexpr int val(int s) {
    return (s>=0) ? val(s-1) : -s;
}
constexpr int f(int x) {
    return 2*val(x);
}

constexpr int Double(int a) {return 2*a;}
constexpr int Sum(int a, int b) {return Double(a)+b;}

#define DOUBLE(a) 2*a
#define SUM(A,B) DOUBLE(A)+B
#define GREETING "Hello World\n"

template <typename T, typename S>
bool can_convert(T t) {
    S s = static_cast<S>(t);
    return t == static_cast<T>(s);
}

struct dynamicShort {
    short *s;
    dynamicShort() {s = new short;}
    ~dynamicShort() {delete s;}
};

void add(dynamicShort& d) {
    *(d.s)++;
}

void vorlesung1217() {
    dynamicShort ds;
    *(ds.s) = 3;
    add(ds);

    constexpr int k = f(509);
    int sum = SUM(3,4);
    int csum = Sum(3,4);

    cout << GREETING << *ds.s << ' ' << k << ' ' << sum << ' ' << csum << endl;

    cout << can_convert<bool, short>(*ds.s) << endl;
}

/*int main() {
    vorlesung1217();
    return 0;
}*/