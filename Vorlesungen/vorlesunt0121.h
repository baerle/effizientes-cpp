#include <iostream>

using namespace std;

class A {
    int value = 2;
    friend class B;
    friend int main();

public:
    /*virtual*/ int getValue() { return value; }
};

class B {
    int value = 1;
public:
    int aValue() {
        A a;
        a.value= 3;
        return a.value;
    }

    /*virtual*/ int getValue() { return value; }
};

class C : public A, public B {
public:
    int getValue() { return A::getValue() + B::getValue(); }
};

void print (A &a) {
    cout << a.getValue() << endl;
}

void print (B &b) {
    cout << b.getValue() << endl;
}

void print (C &c) {
    cout << c.getValue() << endl;
}

class Bruch {
    friend int main();
    friend ostream &operator<<(ostream&, const Bruch&);
private:
    int z,n;
    bool valid;
    void kuerzen();
public:
    Bruch() : Bruch (0,1) {}
    Bruch(int z, int n);
    Bruch operator+(const Bruch& b) const;
    Bruch operator-(const Bruch& b) const;
    Bruch operator*(const Bruch& b) const;
    Bruch operator/(const Bruch& b) const;
    Bruch operator()() { return Bruch(n,z); }
    bool invalid() const;
};

ostream& operator<<(ostream& os, const Bruch& a) {
    return os << a.z << '/' << a.n;
}

Bruch::Bruch(int z, int n) : z(z), n(n) {
    valid = n != 0;
    if (n < 0) {
        this->n = -n;
        this->z = -z;
    }
    kuerzen();
}

inline bool Bruch::invalid() const {
    return valid;
}

Bruch Bruch::operator+(const Bruch &b) const {
    return Bruch(n*b.z+b.n*z, n*b.n);
}

Bruch Bruch::operator-(const Bruch &b) const {
    return Bruch(n*b.z-b.n*z, n*b.n);
}

Bruch Bruch::operator*(const Bruch &b) const {
    return Bruch(z*b.z, n*b.n);
}

Bruch Bruch::operator/(const Bruch &b) const {
    Bruch a (b.n, b.z);
    return a * *this;
}

void Bruch::kuerzen() {
    int a = z;
    int b = n;
    int t = a % b;
    while (t) {
        a = b;
        b = t;
        t = a % b;
    }
    z /= b;
    n /= b;
}

/*int main() {
    B b;
    cout << b.aValue() << endl;
    A a;
    a.value = 4;
    cout << a.value << endl;

    C c {};
    cout << static_cast<A>(c).getValue() << static_cast<B>(c).getValue() << c.getValue() << endl;
    A& aa = c;
    print(aa);
    B& bb = c;
    print(bb);
    print(c);

    Bruch bruch {1,2};
    cout << bruch << endl;
    Bruch bruch2 {2,3};
    cout << bruch2 << endl;

    cout << bruch + bruch2 << endl;
    cout << bruch - bruch2 << endl;
    cout << bruch * bruch2 << endl;
    cout << bruch / bruch2 << endl;
    cout << bruch() << endl;
    //cout << bruch += bruch2 << endl;  // geht nicht, muesste extra ueberladen werden
}*/