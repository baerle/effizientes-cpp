#include <iostream>
#include <cstdarg>
#include <functional>
#include <iomanip>
#include <sstream>

using namespace std;

void printAll(int first, ...) {
    va_list al; // Deklaration der variablen Argumentliste
    va_start(al, first);    // Initialisierung der variablen Argumentliste

    while (true) {
        long long *p = va_arg(al, long long *);
        if (p == nullptr) break;
        stringstream s;
        s << hex << *p;
        cout << s.str() << endl;
    }

    va_end(al);
}

void printAll(initializer_list<string> list) {  // printAll({1,2,3})
    for (auto& s : list) {
        cout << s << endl;
    }
}

int call(const function<int(int, int)> &f, int p) {
    return f(p,p);
}

int call(int (*f)(int, int), int p) {
    return f(p,p);
}

int my(int x, int y) {
    return 2*x + y;
}

int isPositive(int* val) {
    if (val < 0) {  //Fehler
        return -1;
    } else {
        (*val)--;
    }
    return 0;
}

int isPositive(int val) {
    if (val <= 0) {  //Fehler
        throw "Fehler";
    } else {
        return val-1;
    }
}

int test(int val) noexcept {
    try {
        return isPositive(val);
    } catch (...) {
        cerr << "Irgendein Fehler ist aufgetreten";
        return -1;
    }
}

int main(int, char**) {
    printAll(2, "Hallo", "Welt", "!");
    printf("%d %s %c\n", 3, "Test", 'x');
    printAll({"Hallo", "World", "!"});

    cout << call(my, 7) << endl;
    int (*f) (int, int) = my;
    function<int(int,int)> j = my;

    cout << f(6,6) << endl;
    cout << call(j, 7) << endl;

    int z = 3;
    for (int i = z; i > -2; i--) {
        isPositive(&z) == -1 ? cerr << "Fehler" << endl : cout << z << endl;
    }

    z = 3;
    for (int i = 0; i < 3; i++) {
        try {
            cout << (z = test(z)) << endl;
            throw exception {};
        } catch(int k) {
            cerr << "Fehler " << k << endl;
        } catch(const char* e) {
            cerr << e << endl;
        } catch (exception& e) {
            cerr << e.what() << endl;
            //throw;
        } catch (...) {
            cerr << "unspecified exception" << endl;
            //throw -1; //nicht so gut, es können Speicherlecks entstehen
        }
    }

    return 0;
}