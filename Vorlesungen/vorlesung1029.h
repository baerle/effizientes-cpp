#include <iostream>

using namespace std;

double f() {
    static int m = 1; // Beim ersten Aufruf von f, wird m als int global angelegt

    return ++m < 10 ? f() : m;
}

int vorlesung1029() {
    int a = 0;
    int b = {1};
    float c {0.5f};
    float d {1.0};
    int k {};
    int field [] = {1,2,3,4,5};

    cout << a << b << c << d << k << endl;
    for (uint i = 0; i < sizeof(field) / sizeof(int); i++) {
        cout << field[i] << " ";
    }
    cout << endl;

    auto i = 0.5f;
    decltype(i) j = 2*i;
    cout << i << j << endl;

    cout << f() << endl;

    return 0;
}