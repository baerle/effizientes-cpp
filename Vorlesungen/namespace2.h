//
// Created by patrick on 1/14/21.
//

#ifndef C___NAMESPACE2_H
#define C___NAMESPACE2_H

namespace My {
    int i = 10;

    struct out {
        int value;
    };

    // dieser Namespace wird als default eingebunden
    inline namespace V_1_0 {
        int f() {
            return 1000;
        }
    }

    namespace V_2_0 {
        int f() {
            return 2000;
        }
    }
}

#endif //C___NAMESPACE2_H
