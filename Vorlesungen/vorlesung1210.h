#include <iostream>

using namespace std;

struct My {
    int size;
    char *buf;

    My(const string& s) {
        size = s.size();
        buf = new char[s.size()];
        int i = 0;
        for (auto c : s) {
            buf[i++] = c;
        }
    }

    ~My() {
        delete[] buf;
    }

    char* begin() const {
        return buf;
    }

    char* end() const {
        return buf+size;
    }
};

//int main(int argc, int *argv[])
void vorlesung1210()
{
    int field[] = {1,2,3,4,5};

    cout << sizeof(field)/sizeof(int) << endl;

    for (int i= 0; i < sizeof(field)/sizeof(int); i++) {
        cout << field[i] << ' ';
    }
    cout << endl;

    for (int *p = field; p != field+sizeof(field)/sizeof(int); p++) {
        cout << *p << ' ';
    }
    cout << endl;

    for (auto k: field) {
        cout << k << ' ';
    }
    cout << endl;

    My m {"The quick brown jumps over the lazy dog."};

    for (auto &c : m) {
        c = 'a'-c;
        cout << c;
    }
    cout << endl;

    int x = 1;
    x += ++x * x++;
    cout << "x: " << x << endl;

    int i = 0;
    label:
        if (i >= 10) goto end;
        cout << i << endl;
        i++;
        goto label;
    end:
        cout << endl;

    bool zero = false;
    bool one = true;

    cout << ~zero << ' ' << ~one << endl;
}

/*int main() {
    vorlesung1210();
    return 0;
}*/