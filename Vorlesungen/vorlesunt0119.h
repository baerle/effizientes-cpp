#include <iostream>
using namespace std;

#include "klassen.h"
using namespace Klassen;

void Klassen::printBasis(Basis& m) {
        // Auch wenn hier ein My-Object übergeben wird, wird hier die "Basisversion" der Methode value() benutzt.
        // Laut Polymorphismus soll aber die spezielle Methode der abgeleiteten Klasse verwendet werden, solche Methoden nennt man "virtual"
        cout << m.value() << ' ';
}
int My::number {0};

int main() {
    My test {};
    My my {1};
    My *pm = new My(2);

    My copy = my;   //Zuweisungskopieroperation
    My otherCopy(my);   //Kopierkonstruktor
    My &myRef = my; //Referenzzuweisungskopieroperator
    My &anotherRef(my); //Referenzkopierkonstruktor

    my.value(3)->value(2);

    cout << my.value() << pm->value() << copy.value() << otherCopy.value() << myRef.value() << anotherRef.value() << test.value() << endl;
    cout << Klassen::My::count() << endl;

    delete pm;

    printBasis(my);
    //printBasis(static_cast<My>(3)); //Konvertierung in Objekt, wenn Konstruktor nicht explicit

    return 0;
}