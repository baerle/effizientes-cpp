#include <iostream>
#include <array>
#include <vector>

using namespace std;

union Vereinigung {
    int value;
    char data[4];
};

#define MAGENTA 0
enum class Color {
    RED, YELLOW, GREEN, BLUE
};

void printColor(Color c) {
    switch(c) {
        case Color::RED:
            cout << "red" << endl;
            break;
        case Color::YELLOW:
            cout << "yellow" << endl;
            break;
        case Color::GREEN:
            cout << "green" << endl;
            break;
        case Color::BLUE:
            cout << "blue" << endl;
            break;
        //case MAGENTA:
        default:
            cout << "magenta" << endl;
            break;
    }
}

int sum(vector<int> &v) {
    int sum = 0;
    for (int x : v) {
        sum += x;
    }
    return sum;
}

void vorlesung1203() {
    //Vereinigung v {};

    union {
        int value;
        char data[4];
    };

    //v.value = (65<<16) + (65<<8) + (65);
    char a = 'A';
    for (auto& c : /*v.*/data) c = a++;
    /*v.*/data[3] = '\0';

    cout << /*v.*/data << endl;
    cout << value << endl;

    Color c {static_cast<Color>(MAGENTA)}; //mit 0 initialisiert, also RED

    printColor(c);

    // Die Datentypen aus der Standardbibliothek werden klein geschrieben
    // Die Größe muss bei der Deklaration bekannt sein, wie bei normalen array auch
    array<int,2> field {-1, -1};
    vector<int> field2 {-1, -1};

    for (auto &x: field) {
        x++;
        cout << x << endl;
    }

    for (uint i = 0; i < field.size(); i++) {
        cout << field[i] << endl;
    }

    cout << sum(field2) << endl;
}

/*int main() {
    vorlesung1203();
    return 0;
}*/