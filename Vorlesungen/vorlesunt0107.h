#include <iostream>

using namespace std;

constexpr auto test(int = 0) -> int;

void print(int x = 0, int y = 2) {
    cout << x << ' ' << y << endl;
}

int main(int argc, char *argv[]) {
    print(2,test());
}

constexpr inline auto test(int x) -> int {
    return x;
}