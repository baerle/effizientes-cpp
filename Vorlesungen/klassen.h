#ifndef C___KLASSEN_H
#define C___KLASSEN_H

#include <iostream>

namespace Klassen {
    class Basis {
    protected:
        double _value = 1.5;
    public:
        // Diese Methode soll dem Polymorphismus genügen
        virtual double value() {
            return _value;
        }
    };

    class My : public Basis {
        int _value = -1;
        static int number;
        friend int main();

    public:
        static int count() { return number; }
        double value() const {
            return _value + 2 * Basis::_value;
        }
        My* const value(int value) {
            this->_value = value;
            return this;
        }
        /*My& const value(int value) {
            this->_value = value;
            return *this;
        }*/
        My() = default; //Konstruktor, der als Standardkonstruktor generiert wird
        explicit My(int value):_value(value) { number++; }

        ~My() {
            std::cout << "Object deleted" << endl;
            number--;
        };
    };

    void printBasis(Basis &m);
}

#endif //C___KLASSEN_H
