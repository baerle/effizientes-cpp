#include <iostream>
#include <typeinfo>
#include <vector>

using namespace std;

void print_int(int k) {
    cout << k << endl;
}

struct MyData {
    char a;
    int b;
    double c;
    MyData(int a, char b, double c): a(b), b(a), c(c) {}
};

int get_val(double x) {
    return int(x * 100);
}

auto getVal(double x) -> int {return x * 100;}

// Funktionen höherer Ordnung, sind Funktionen, die eine Funktion als Parameter erwarten
void my_val(int (*p)(double)) {
    cout << p(10) << endl;
}

template <typename T>
void get_value(T &f) {
    cout << f(10) << endl;
}

void vorlesung1222() {
    auto k = {0}; //std::initializer_list<T>
    cout << typeid(k).name() << endl;

    print_int({0});

    const char *greet = "Greeting";
    string s {greet}; //wird vom Compiler zu einem Konstruktoraufruf gemacht

    auto v = vector<int> {1,2,3,4,5}; //Kopierinitialisierung, zunächst wird ein temp. Obj. erstellt, welches anschl. kopiert wird
    vector<int> w {6,7,8,9,0}; //Konstruktorvariante, ohne temp. Objekt und kopieren

    MyData m {10, 'b', 0.5};

    // Deklaration/Datentyp eines Funktionszeigers bestehend aus: <Rückgabetyp> (<Zeigerdeklaration>) (<Parameterliste>);
    // p ist ein Pointer auf eine Funktion, die int liefert und double übergeben bekommt
    int (*p)(double) = get_val;
    cout << "function pointer: " << p(0.5) << endl;
    cout << "Super: " << get_val(1.219) << endl;

    int u = 10;
    vector<int> a {10,20,50,100};
    auto my_lambda = [&] (double x) mutable -> int {
        u++;
        for (auto y : a) {
            x /= y;
        }
        if (x < 0)
            return (int) (-x * 100);
        return (int) (x * 100);
    };
    cout << my_lambda(100000) << endl;
    cout << u << endl;

    cout << typeid(my_lambda).name() << endl;

    my_val(getVal);
    get_value(my_lambda);
}

int main() {
    vorlesung1222();
    return 0;
}