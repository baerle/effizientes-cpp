#include <iostream>
#include <thread>

using namespace std;

void sum_up();
void sum_upThread();

int val = 0;
volatile bool flag[] {false, false};
volatile int turn;

int main() {
    thread t0 {sum_upThread};
    sum_up();
    t0.join();

    cout << val << endl;

    return 0;
}

inline void increment() {
    ++val;
}

void sum_up() {
    for (int i = 0; i < 2000000; i++) {
        flag[1] = true;
        turn = 0;
        while (flag[0] && turn == 0); // Warte
        //while (true) if (!flag[0] || turn != 0) break;
        increment();
        flag[1] = false;
    }
}

void sum_upThread() {
    for (int i = 0; i < 2000000; i++) {
        flag[0] = true;
        turn = 1;
        while (flag[1] && turn == 1); // Warte
        //while (true) if (!flag[1] || turn != 1) break;
        increment();
        flag[0] = false;
    }
}