#include <iostream>

using namespace std;

const int rounds = 2000000;
static int i = 0;
static pthread_mutex_t mutex {};

void increment() {
    i++;
}

void *incSafe(void*) {
    for (int i = 0; i < rounds; i++) {
        pthread_mutex_lock(&mutex);
        increment();
        pthread_mutex_unlock(&mutex);
    }
}

int main(int, char**) {
    pthread_t i1 {};
    pthread_create(&i1, nullptr, incSafe, nullptr);
    incSafe(nullptr);
    pthread_join(i1, nullptr);

    cout << i << endl;

    return 0;
}