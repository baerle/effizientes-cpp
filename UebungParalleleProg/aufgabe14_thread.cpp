#include <iostream>
#include <thread>
#include <mutex>

using namespace std;

const int rounds = 2000000;
static int i = 0;
static mutex mtx {};

void increment() {
    i++;
}

void incSafe() {
    for (int i = 0; i < rounds; i++) {
        mtx.lock();
        increment();
        mtx.unlock();
    }
}

int main(int, char**) {
    thread i1 {incSafe};
    incSafe();
    i1.join();

    cout << i << endl;

    return 0;
}