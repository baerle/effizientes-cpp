#include <iostream>
#include <thread>
#include <semaphore.h>

using namespace std;

const int rounds = 2000000;
static int i = 0;
static sem_t semaphore {};

void increment() {
    i++;
}

void incSafe() {
    for (int i = 0; i < rounds; i++) {
        sem_wait(&semaphore);
        increment();
        sem_post(&semaphore);
    }
}

int main(int, char**) {
    thread i1 {incSafe};
    sem_init(&semaphore, 0, 1);
    incSafe();
    i1.join();

    cout << i << endl;

    return 0;
}