#include <iostream>
#include <thread>
#include <semaphore.h>
#include <time.h>
#include <chrono>
#include <vector>

using namespace std;

int used = 0;
static sem_t sem {};

class Auto {
public:
    int id;
    Auto(int id) : id(id) {
        this_thread::sleep_for(chrono::seconds(rand() % 5));
        enter();
        this_thread::sleep_for(chrono::seconds(rand() % 5));
        leave();
    }

    void enter() {
        sem_wait(&sem); //p()
        ::used++;
        cout << id << " entered: " << ::used << endl;
    }

    void leave() {
        ::used--;
        sem_post(&sem); //v()
        cout << id << " left: " << ::used << endl;
    }
};

static void make(int i) {
    new Auto(i);
}

const int size = 30;

int main() {
    sem_init(&sem, 0, 10);
    srand(time(nullptr));

    vector<thread> t;

    for (int i = 0; i < size; i++) {
        t.emplace_back(thread (make, i));
    }

    for (int i = 0; i < size; i++) {
        t[i].join();
    }
}