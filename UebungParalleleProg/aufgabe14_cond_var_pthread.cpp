#include <iostream>
#include <pthread.h>

using namespace std;

const int rounds = 2000000;
static volatile int i = 0;
static pthread_mutex_t mtx;
static pthread_cond_t cond;
static int turn = 0;
const static int threads = 3;

void increment() {
    i++;
}

void *incSafe(void *k) {
    int *x = (int *) k;
    for (int a = 0; a < rounds; a++) {
        pthread_mutex_lock(&mtx);
        while (turn == *x) {
            pthread_cond_wait(&cond, &mtx);
        }
        increment();
        turn = (turn+1 % threads);
        pthread_mutex_unlock(&mtx);
        pthread_cond_broadcast(&cond);
    }
    return nullptr;
}

int main(int, char**) {
    pthread_mutex_init(&mtx, nullptr);
    pthread_cond_init(&cond, nullptr);
    pthread_t i1;
    pthread_t i2;
    int a = 0;
    int b = 1;
    int c = 2;
    pthread_create(&i1, nullptr, incSafe, &b);
    pthread_create(&i2, nullptr, incSafe, &c);
    incSafe(&a);
    pthread_join(i1, nullptr);
    pthread_join(i2, nullptr);
    pthread_mutex_destroy(&mtx);
    pthread_cond_destroy(&cond);

    cout << i << endl;

    return 0;
}