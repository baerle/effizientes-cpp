#include <iostream>
#include <thread>
#include <mutex>
#include <condition_variable>

using namespace std;

const int rounds = 2000000;
static int i = 0;
static mutex mtx {};
static condition_variable cond;
static int turn = 0;
const static int threads = 3;

void increment() {
    i++;
}

void incSafe(int k) {
    for (int i = 0; i < rounds; i++) {
        unique_lock<mutex> lk(mtx);
        while (turn == k) {
            cond.wait(lk);
        }
        increment();
        turn = (turn+1 % threads);
        lk.unlock();
        cond.notify_all();
    }
}

int main(int, char**) {
    thread i1 {incSafe, 1};
    thread i2 {incSafe, 2};
    incSafe(0);
    i1.join();
    i2.join();

    cout << i << endl;

    return 0;
}