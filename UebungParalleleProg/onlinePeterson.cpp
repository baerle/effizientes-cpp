#include <pthread.h>
#include <unistd.h>
#include <iostream>
#include <string>
#include <atomic>

using namespace std;

atomic<int> turn {};
int flag[2];

int count {0};

void* f0(void* arg) {
    for (int i = 0; i < 2000000; i++) {
        // Signal that this thread wants to enter the critical section.
        flag[0] = 1;
        // Signal to the other thread that it is their turn.
        turn.store(1,memory_order_acq_rel);
        while(flag[1] && turn.load(memory_order_acq_rel) == 1);
        ++count;
        flag[0] = 0;
    }
}

void* f1(void* arg) {
    for (int i = 0; i < 2000000; i++) {
        flag[1] = 1;
        // Signal to the other thread that it is their turn.
        turn.store(0,memory_order_acq_rel);
        while(flag[0] && turn.load(memory_order_acq_rel) == 0);
        ++count;
        flag[1] = 0;
    }
}

int main() {
    // A POSIX thread has two main components: an object of type `pthread_t`
    // which represents the thread and a function pointer of type
    // `void* (*)(void*)` which will be the entry point of the thread.
    pthread_t t0, t1;

    // Creates new threads. The second argument is a pointer to a
    // `pthread_attr_t`, if `NULL` the thread is created with default attributes.
    // The last argument is the argument that is given to the thread's entry
    // point function, unused in this example.
    pthread_create(&t0, NULL, f0, NULL);
    pthread_create(&t1, NULL, f1, NULL);

    // Yes, I could have just created one thread.
    pthread_join(t0, nullptr);
    pthread_join(t1, nullptr);
    cout << count << endl;
}