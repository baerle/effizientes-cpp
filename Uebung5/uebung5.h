#include <iostream>
#include <random>
#include <ctime>

using namespace std;

void uebung51() {
    const int plaetze = 78;
    const int N = 100;
    int aerger = 0;
    int belegungDurchschnitt = 0;

    srand(time(nullptr));

    for (int n = 0; n < N; n++) {
        int besucher = 0;
        for (int i = 0; i < plaetze; i++) {
            if (random() % 20) {   // 5% := 1/20
                besucher++;
            }
        }
        if (besucher > plaetze) {
            aerger++;
            belegungDurchschnitt += plaetze;
        } else {
            belegungDurchschnitt += besucher;
        }
    }

    cout << "Ärger: " << aerger << endl;
    cout << "Durchschnittliche Belegung: " << belegungDurchschnitt/N << endl;
}