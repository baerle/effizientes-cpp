#include <iostream>
#include <string>
#include <limits>
#include "Vorlesungen/vorlesung1029.h"
#include "Vorlesungen/vorlesung1105.h"
#include "Vorlesungen/vorlesung1119.h"
#include "Vorlesungen/vorlesung1126.h"
#include "Vorlesungen/vorlesung1203.h"
#include "Vorlesungen/vorlesung1210.h"
#include "Vorlesungen/vorlesung1217.h"
#include "Vorlesungen/vorlesung1222.h"
#include "Uebung1/quersumme.h"
#include "Uebung2/uebung2.h"
#include "Uebung3/uebung3.h"
#include "Uebung4/uebung4.h"
#include "Uebung5/uebung5.h"

using namespace std;

int main(int argc, char *argv[]) {
    /*cout << "Hello, World!" << endl;
    cout << "int " << sizeof(int) << endl;
    cout << "long " << sizeof(long) << endl;
    cout << "long long " << sizeof(long long) << endl;
    cout << "short " << sizeof(short) << endl;
    cout << "char " << sizeof(char) << endl;
    cout << "float " << sizeof(float) << endl;
    cout << "double " << sizeof(double) << endl;
    cout << "long double " << sizeof(long double) << endl;
    cout << "utf-8 " << sizeof(char16_t ) << endl;
    cout << numeric_limits<int>::min() << " " << numeric_limits<int>::max() << endl;
    cout << numeric_limits<float>::min() << " "  << numeric_limits<float>::max() << endl;
    cout << numeric_limits<double>::min() << " "  << numeric_limits<double>::max() << endl;
    cout << numeric_limits<long double>::min() << " "  << numeric_limits<long double>::max() << endl << endl;

    string str = "Dies ist ein C-String, er wird durch ein Null-Byte terminiert!";
    const char *cstr = str.c_str();

    for (char i : str) {
        cout << i;
    }

    for (int i = 0; str[i]; cout << str[i++]);
    cout << endl;

    cout << str << endl;

    cout << str.c_str() << endl;

    int i = 5;

    if (int z = i) {
        z *= 2;
        cout << z << endl;
    }
    cout << i << endl << endl;*/

    //quersumme_int();

    //vorlesung1029();
    //vorlesung1105();
    //vorlesung1119();
    //vorlesung1126();
    //vorlesung1203();

    //uebung21(argc, argv);
    //uebung22_char(argc, argv);
    //uebung22_site2(argc, argv);
    //uebung23();
    //uebung24(argc, argv);

    //uebung31();
    //uebung32();
    //uebung33();
    //uebung34();

    //uebung41();
    //uebung42();
    //uebung422();
    //uebung43();
    //uebung44();

    uebung51();

    return 0;
}
