#include <iostream>
#include <cstdlib>
#include <fstream>
#include <string>
#include <cctype>

using namespace std;

void uebung21(int argc, char *argv[]) {
    if (argc == 1) {
        cerr << "Aufruf: " << argv[0] << " <zahl> [<zahl>...]" << endl;
        exit(1);
    }

    long sum = 0;
    for (int i = 1; i < argc; ++i) {
        sum += atoi(argv[i]);
    }

    cout << endl << "Summe: " << sum << endl;
}

void uebung22_char(int argc, char *argv[]) {
    if (argc != 1) {
        string filename = argv[1];
        ifstream file;
        file.open(filename);

        while (!file.eof()) {
            cout << file.get();
        }
    } else {
        cerr << "No file as parameter given" << endl;
        exit(1);
    }
}

void uebung22_line(int argc, char *argv[]) {
    if (argc != 1) {
        string i;
        string filename = argv[1];
        ifstream file;
        file.open(filename);

        while (!file.eof()) {
            getline(file, i);
            cout << i;
        }
        file.close();
    } else {
        cerr << "No file as parameter given" << endl;
        exit(1);
    }
}

void uebung22_site(int argc, char *argv[]) {
    if (argc != 1) {
        string filename = argv[1];
        ifstream file;
        file.open(filename);
        int pos = 0;

        while (!file.eof()) {
            string line;
            file.seekg(pos);
            for (int i = 0; i < 2; ++i) {
                getline(file, line);
                cout << line << endl;
            }
            pos = file.tellg();
            getchar();
        }
        file.close();
    } else {
        cerr << "No file as parameter given" << endl;
        exit(1);
    }
}

void uebung22_site2(int argc, char *argv[]) {
    if (argc != 1) {
        ifstream file {argv[1]};
        //string filename = argv[1];
        //file.open(filename);

#define BUFSIZE 128

        char buf[BUFSIZE] {};

        while (!file.eof()) {
            file.read(buf, BUFSIZE);
            cout << string {buf, (size_t) file.gcount()};

            cin.ignore(1024, '\n');
        }

    } else {
        cerr << "No file as parameter given" << endl;
        exit(1);
    }
}

void uebung23() {
    string s;
    while(getline(cin, s)) {
        for(ulong i = s.length()-1; i >= 0; i--) {
            cout << (char) toupper(s[i]);
        }
        cout << endl;
    }
}

void uebung24(int argc, char *argv[]) {
#define SIZE_N 256
    ifstream file {argv[1]};

    int c[SIZE_N] {};

    while (!file.eof()) {
        c[file.get()]++;
    }

    int max = 0, maxIndex = -1;
    for (int i = 0; i < 10; ++i) {
        for (int j = 0; j < SIZE_N; j++) {
            if (c[j] > max) {
                max = c[j];
                maxIndex = j;
            }
        }
        cout << (char) maxIndex << ": " << max << endl;
        c[maxIndex] = 0;
        max = 0;
    }
}