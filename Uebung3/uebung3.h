#include <iostream>
#include <ctime>
#include <random>
#include <cmath>
#include <fstream>

using namespace std;

bool randomMisesProblem(ushort x);

int ggT(uint a, uint b);

void uebung31() {
#define SIZE 10000

    bool experiment[SIZE]{};
    for (int i = 0; i < SIZE; ++i) {
        experiment[i] = randomMisesProblem(i);
    }

    //sort(experiment, experiment + SIZE);
    for (int i = 0; i < SIZE - 1; ++i) {
        //cout << experiment[i] << " ";
        if (experiment[i]) {
            cout << endl << endl << "probability: " << 1 - (double) i / SIZE << endl;
            return;
        }
    }
    cout << "probability: " << 0 << endl;
}

bool randomMisesProblem(ushort x) {
#define DAYS 365
#define N_PERSON 30
    srand(time(nullptr) + x);

    // zufällige Stickprobe erstellen
    ushort persons[N_PERSON]{};

    for (int i = 0; i < N_PERSON; ++i) {
        persons[i] = (random() % DAYS) + 1;
    }

    //cout << endl << endl;
    //sort(persons, (persons + N_PERSON), less<>());
    for (int i = 0; i < N_PERSON - 1; ++i) {
        //cout << persons[i] << " ";
        if (persons[i] == persons[i + 1]) {
            return true;
        }
    }
    return false;
}

void uebung32() {
    const ushort N{100};

    ushort nums[N];

    for (int i = 0; i < N; ++i) {
        nums[i] = i + 2;
    }

    ushort prim;
    //Sieb des Erastothenes
    for (int i = 0; i < sqrt(N); ++i) {   // aktuelle Primzahl
        if (nums[i] == 0) continue;
        prim = nums[i];
        cout << prim << " ";

        for (int j = 0; j < N; ++j) {   //überprüfe gesamtes Array und entferne Divisoren
            if (nums[j] != 0 && nums[j] % prim == 0) {
                nums[j] = 0;
            }
        }
    }
    for (int i = prim + 1; i < N; ++i) {
        if (nums[i] != 0) {
            cout << nums[i] << " ";
        }
    }
}

void uebung33() {
    const uint N{10};
    uint matrix[N][N];

    for (int i = 0; i < N; ++i) {
        for (int j = 0; j < N; ++j) {
            matrix[i][j] = ggT(i + 1, j + 1) == 1;
            cout << matrix[i][j] << " ";
        }
        cout << endl;
    }
}

int ggT(uint a, uint b) {
    uint h;
    if (a == 0) return a;
    if (b == 0) return b;

    do {
        h = a % b;
        a = b;
        b = h;
    } while (b != 0);

    return a;
}

void uebung34() {
    ifstream fileMatrix1("../Übung3/matrix1.txt");
    ifstream fileMatrix2("../Übung3/matrix2.txt");

    ushort rows1 = fileMatrix1.get() - '0';
    fileMatrix1.get(); // Space
    ushort cols1 = fileMatrix1.get() - '0';
    //cout << rows1 << ":" << cols1 << endl;

    ushort rows2 = fileMatrix2.get() - '0';
    fileMatrix2.get();
    ushort cols2 = fileMatrix2.get() - '0';

    if (rows1 == cols2) {
        ushort row, col;

        ushort matrix1[rows1][cols1];
        for (int i = 0; i < rows1; ++i) {
            for (int j = 0; j < cols1; ++j) {
                int val{0};
                do {
                    val = fileMatrix1.get() - '0';
                } while (val < 0);
                matrix1[i][j] = val;
                //cout << val << " ";
            }
            //cout << endl;
        }

        ushort matrix2[rows2][cols2];
        for (int i = 0; i < rows2; ++i) {
            for (int j = 0; j < cols2; ++j) {
                int val{0};
                do {
                    val = fileMatrix2.get() - '0';
                } while (val < 0);
                matrix2[i][j] = val;
                //cout << val << " ";
            }
            //cout << endl;
        }

        ushort matrix[rows1][cols2];
        col = 0, row = 0;

        for (int x = 0; x < rows1; ++x) {   // matrix 1 nach unten
            for (int i = 0; i < cols2; ++i) {   // matrix2 nach rechts
                int val{0};
                for (int j = 0; j < cols1; ++j) {   // matrix1 nach rechts
                        val += matrix1[x][j] * matrix2[j][i];
                }
                matrix[row][col++] = val;
            }
            row++;
            col = 0;
        }

        for (int i = 0; i < rows1; ++i) {
            for (int j = 0; j < cols2; ++j) {
                cout << matrix[i][j] << " ";
            }
            cout << endl;
        }
    } else cout << "Not two matrixes!";

    fileMatrix1.close();
    fileMatrix2.close();
}