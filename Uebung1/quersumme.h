#include <iostream>

int calcQuersumme(int num);

using namespace std;

int quersumme_int() {
    cout << "Please insert a number" << endl;
    int input;
    while (!cin.eof()) {
        cin >> input;
        if (!cin.fail()) {
            cout << "Quersumme: " << calcQuersumme((int) input) << endl;
        } else {
            cout << "no digit" << endl;
            // Rücksetzen des Fehlerflags
            cin.clear();
            // Weglesen des Eingabepuffers
            string s;
            cin >> s;
        }
    }

    return 0;
}

int quersumme_chars() {
    cout << "Please insert a number" << endl;
    int quersumme = {0};
    char input;
    while (!cin.eof()) {
        cin >> input;
        if (!cin.fail()) {
            if (isdigit(input)) {   // input >= '0' && input <= '9'
//              cout << input[i] << "-" << endl;
                quersumme += input - '0';
            } else {
                cout << "no digit" << endl;
            }
        }
    }
    cout << "Quersumme: " << quersumme << endl;

    return 0;
}

int quersumme_string() {
    cout << "Please insert a number" << endl;
    string input;
    while (!cin.eof()) {
        cin >> input;
        if (!cin.fail()) {
            int quersumme = 0;
            bool error = false;
            for(char i : input) {
                if (i != 0 && isdigit(i) && !error) {
//                    cout << input[i] << "-" << endl;
                    quersumme += i - '0';
                } else {
                    cout << "no digit" << endl;
                    error = true;
                    break;
                }
            }
            cout << "Quersumme: " << quersumme << endl;
        }
    }

    return 0;
}

int calcQuersumme(int num) {
    int quersumme = {0};
    do {
        quersumme += num % 10;
    } while (num /= 10);

    return quersumme;
}
